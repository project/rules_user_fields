# Rules User Fields


## Introduction

This module extends the "Rules" module.
Today the "Rules" module has the following restriction:
If you create a new 'User' in a 'Rule' then you can't get access
to its custom fields in this 'Rule'. This module solves this problem.
Possible use case: you can create new users with the "Rules" module and populate
their fields with a data obtained using the "Webform" module.

**Note:
if you don't create a new user with Rules, but need to get access to custom
fields of a user, then instead of this module you can use
the "Direct input mode" or create the "Entity is of bundle" condition and
populate fields with "user", see:
https://www.drupal.org/project/rules_user_fields/issues/3257625#comment-14556182
https://www.drupal.org/project/rules_user_fields/issues/3284425#comment-14549216**

## Requirements

This module does not requires additional modules.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings.
There is no configuration.


## How to Use

The module enables to get access to all fields (including custom) of a User
entity from Rules. For doing this the module provides special Rule action called
"Get access to user fields".
To get access to a user fields you need just add this action after creation of
a new user.

Do the following:

1. Add the "Get access to user fields" action (you will find it in User section
of the Rules actions list).
2. Click on the "Switch to data selection" button and select the entity of newly
created user.
3. Save the Rule.

After that all fields of the selected User entity will be available from the
data selector. And you will can to use them in others actions of this Rule.

## Maintainers

- Andrey Vitushkin - [wombatbuddy](https://www.drupal.org/u/wombatbuddy)
